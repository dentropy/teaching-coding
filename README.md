# Let's learn to code

## Learn bash basics in 3 minutes

``` bash
# THIS DOES NOTHING
pwd # pwd shows where you are in your file system
ls # Look at the files and directories
mkdir hello # make a directory
ls # check if directory hello is there
cd hello # the cd command stands for change directory
touch world.txt # Creates a file
echo "I Like Pie" # Prints some stuff
echo "I Like Pie" > world.txt # Write to file
echo "I eat cows" >> world.txt # Append to file
cat world.txt
nano world.txt
# ctrl + o to write file
# ctrl + x to exit

mv world.txt ..
ls
cd ..
ls
cp world.txt hello
ls
ls hello
rm world.txt
ls
```

## How to setup SSH

``` bash
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
ssh-add ~/.ssh/id_rsa
cat ~/.ssh/id_rsa.pub
## Copy to gitlab / github
```

## Learn git basics in three minutes

``` bash
git # you should not get an error

# If you have an error
sudo apt update
sudo apt install git

mkdir tmp_project
cd tmp_project
git init


git config --global user.name "Your Name"
git config --global user.email you@example.com


echo "# Mah Project" > README.md
cat README.md
git add README.md
git status
git commit -m "First commit"
git status

git log
```

## [Learning Python in two weeks](https://github.com/ubarredo/LearnPythonTheHardWay)

### Simple python3 projects

1. A text based adventure game
2. X's and O's on the terminal
3. Terminal based calculator
4. Calculate factorial using recursion
5. Database using JSON, CSV, or txt files
6. Read and write a file
7. Rock paper sisors
8. Password Generator
9. Guess number game, warm hot, etc...
10. Get time in differnt timezones

## If you know how to code learn python [here](https://learnxinyminutes.com/docs/python/)

## Learning HTML/CSS/Javscript go [here](https://www.w3schools.com/)

## I am going to try and teach javascript from [here](https://learnxinyminutes.com/docs/javascript/)